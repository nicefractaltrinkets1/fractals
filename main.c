/* Calculate various fractals */

/* This program is optimized for precision not speed. */

/* Output is a 16 bit per color RGB PNG file containing the iteration
 * value as 48 bit unsigned integer, in little endian 16 bit pieces.
 */

/* TODO
 * port to C++ to make it easier to maintain for more fractal types?
 */

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <getopt.h>
#include <bsd/string.h>

#include "common.h"

parameters_t parameters = {
	NULL, NULL, NULL,
	1920, 1080,
	1,
	+1000.0, -1000.0,
	+1000.0, -1000.0,
	0,
	2
};

int main(int argc, char *argv[])
{
	int command = 0;

	struct option longoptions[] = {
		{ "x1", 1, NULL, '\001' },
		{ "x2", 1, NULL, '\002' },
		{ "y1", 1, NULL, '\003' },
		{ "y2", 1, NULL, '\004' },
		{ "maxiter", 1, NULL, '\005' },
		{ "xres", 1, NULL, '\006' },
		{ "yres", 1, NULL, '\007' },
		{ "threads", 1, NULL, '\010' },
		{ "exponent", 1, NULL, '\014' },
		{ "histogram", 0, &command, '\100' },
		{ "setpalette", 0, &command, '\101' },
		{ "in", 1, NULL, '\011' },
		{ "out", 1, NULL, '\012' },
		{ "palette", 1, NULL, '\013' },
		{ 0, 0, 0, 0 }
	};
	job_t *jobs = NULL;
	pixel_t *buf = NULL;

	/* parameter parsing */
	while(1) {
		int i;
		i = getopt_long(argc, argv, "", longoptions, NULL);
		if (i == -1) break;
		switch(i) {
			case '\001':
				parameters.x1 = strtold(optarg, NULL);
				break;
			case '\002':
				parameters.x2 = strtold(optarg, NULL);
				break;
			case '\003':
				parameters.y1 = strtold(optarg, NULL);
				break;
			case '\004':
				parameters.y2 = strtold(optarg, NULL);
				break;
			case '\005':
				parameters.maxiter = strtoull(optarg, NULL, 0);
				break;
			case '\006':
				parameters.xres = strtoul(optarg, NULL, 0);
				break;
			case '\007':
				parameters.yres = strtoul(optarg, NULL, 0);
				break;
			case '\010':
				parameters.threads = strtoul(optarg, NULL, 0);
				break;
			case '\011':
				parameters.infile = optarg;
				break;
			case '\012':
				parameters.outfile = optarg;
				break;
			case '\013':
				parameters.palfile = optarg;
				break;
			case '\014':
				parameters.exponent = strtoul(optarg, NULL, 0);
				break;
		}
	}

	/* histogram */
	if ('\100' == command) {
		unsigned long xpixel, ypixel;
		pixel_t *buf = NULL;
		unsigned long long iter;
		unsigned long long maxiter = 0;
		unsigned long long miniter = 0xffffffffffff;
		unsigned long long *histo = NULL;
		int res;

		res = loadpng(parameters.infile, &parameters, &buf);
		if (!res) {
			fprintf(stderr, "loadpng %d\n", res);
			exit(1);
		}
		for (ypixel = 0; ypixel < parameters.yres; ypixel++) {
			for (xpixel = 0; xpixel < parameters.xres; xpixel++) {
				iter = buf[ypixel * parameters.yres + xpixel].r;
				iter = iter | (unsigned long long) buf[ypixel * parameters.yres + xpixel].g >> 16;
				iter = iter | (unsigned long long) buf[ypixel * parameters.yres + xpixel].b >> 32;
				if (iter < miniter) miniter = iter;
				if (iter > maxiter) maxiter = iter;
			}
		}
		histo = malloc((maxiter-miniter+1)*sizeof(unsigned long long));
		memset(histo, 0, (maxiter-miniter+1)*sizeof(unsigned long long));
		if (!histo) {
			fprintf(stderr, "malloc histo\n");
			exit(1);
		}
		for (ypixel = 0; ypixel < parameters.yres; ypixel++) {
			for (xpixel = 0; xpixel < parameters.xres; xpixel++) {
				iter = buf[ypixel * parameters.yres + xpixel].r;
				iter = iter | (unsigned long long) buf[ypixel * parameters.yres + xpixel].g >> 16;
				iter = iter | (unsigned long long) buf[ypixel * parameters.yres + xpixel].b >> 32;
				histo[iter-miniter]++;
			}
		}
		for(iter = miniter; iter <= maxiter; iter++) {
			printf("%llu, %llu\n", iter, histo[iter-miniter]);
		}
		exit(0);
	}

	/* setpalette */
	if ('\101' == command) {
		unsigned long xpixel, ypixel;
		unsigned long long iter, maxpalette, *palette;
		int res;

		res = loadpalette(parameters.palfile, &palette, &maxpalette);
		if (!res) {
			fprintf(stderr, "loadpalette %d\n", res);
			exit(1);
		}

//for(iter = 0; iter <= maxpalette; iter++)
//printf("%llu %llu\n", iter, palette[iter]);

		res = loadpng(parameters.infile, &parameters, &buf);
		if (!res) {
			fprintf(stderr, "loadpng %d\n", res);
			exit(1);
		}

		for (ypixel = 0; ypixel < parameters.yres; ypixel++) {
			for (xpixel = 0; xpixel < parameters.xres; xpixel++) {
				iter = buf[ypixel * parameters.xres + xpixel].r;
				iter = iter | (unsigned long long) buf[ypixel * parameters.xres + xpixel].g << 16;
				iter = iter | (unsigned long long) buf[ypixel * parameters.xres + xpixel].b << 32;
				if (iter > maxpalette) iter = maxpalette;
				buf[ypixel * parameters.xres + xpixel].r = palette[iter];
				buf[ypixel * parameters.xres + xpixel].g = palette[iter] >> 16;
				buf[ypixel * parameters.xres + xpixel].b = palette[iter] >> 32;
			}
		}

		res = savepng(parameters.outfile, &parameters, buf);
		if (!res) {
			fprintf(stderr, "savepng %d\n", res);
			exit(1);
		}
		exit(0);
	}

	/* calculate the fractal */

	/* sanity checks */
	{
		int abort = 0;
		if (parameters.x1 >= parameters.x2) { fprintf(stderr, "--x1 must be < --x2\n"); abort = 1; }
		if (parameters.y1 >= parameters.y2) { fprintf(stderr, "--y1 must be < --y2\n"); abort = 1; }
		if (0 == parameters.maxiter) { fprintf(stderr, "maxiter must be > 0\n"); abort = 1; }
		if (parameters.threads < 1) { fprintf(stderr, "--threads must be >= 1\n"); abort = 1; }
		if (parameters.threads > 200) { fprintf(stderr, "--threads must be <= 200\n"); abort = 1; }
		if (abort) exit(1);
	}

	/* allocate and fill job data */
	{
		unsigned long i;

		jobs = malloc(sizeof(job_t) * parameters.yres);
		buf = malloc(parameters.xres * parameters.yres * sizeof(pixel_t));
		//FIXME: error check
		for(i = 0; i < parameters.yres; i++) {
			jobs[i].parameters = &parameters;
			jobs[i].render = &mandelbrot; //FIXME allow others
			jobs[i].state = job_ready;
			jobs[i].y = parameters.y1 + i * (parameters.y2 - parameters.y1) / parameters.yres;
			jobs[i].line = &buf[parameters.xres * i];
			//FIXME: error check
		}
	}

	/* render everything */
	{
		unsigned long i;
		unsigned long running = 0;
		long ready = -1;
		int res;

		while (ready < 0) {
			running = 0;
			for(i = 0; i < parameters.yres; i++) {
				if ((ready < 0) && (job_ready == jobs[i].state)) ready = i;
				if (job_running == jobs[i].state) running++;
				if (running > parameters.threads) break;
			}
			if (running < parameters.threads) {
				if (ready < 0) break; /* nothing more to do! */
				jobs[ready].state = job_running;
				pthread_create(&jobs[ready].id, NULL, &worker, &jobs[ready]);
				//FIXME error check!!!
				ready = -1;
			} else {
				/* everybody already working */
//printf("BUSY ready %ld running %lu\n", ready, running);
//				sleep(1);
				ready = -1;
			}
		}
		/* write to file */
		res = savepng(parameters.outfile, &parameters, buf);
		if (!res) {
			fprintf(stderr, "savepng %d\n", res);
printf("out %s\n", parameters.outfile);
			exit(1);
		}
	}
	return 0;
}
