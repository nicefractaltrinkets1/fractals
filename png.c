/* PNG file handling via libpng */

#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <png.h>

#include "common.h"

int savepng(char *fn, parameters_t *parameters, pixel_t *buf)
{
	int res;
	png_image image;

	memset(&image, 0, sizeof(png_image));
	image.version = PNG_IMAGE_VERSION;
	image.width = parameters->xres;
	image.height = parameters->yres;
	image.format = PNG_FORMAT_LINEAR_RGB;
	image.flags = 0;

	res = png_image_write_to_file(&image, fn, 0, buf, 0, NULL);

	return res;
}

int loadpng(char *fn, parameters_t *parameters, pixel_t **buf)
{
	png_image image;
	int res;

	memset(&image, 0, sizeof(png_image));
	image.version = PNG_IMAGE_VERSION;

  	res = png_image_begin_read_from_file(&image, fn);
	if (!res) {
		if (errno) perror("png_image_begin_read_from_file");
		else fprintf(stderr, "png_image_begin_read_from_file %d\n", res);
		return res;
	}
	if (PNG_FORMAT_LINEAR_RGB != image.format) {
		fprintf(stderr, "Image must be 16 bit linear RGB\n");
		return res;
	}
	parameters->xres = image.width;
	parameters->yres = image.height;
	*buf = malloc(parameters->xres * parameters->yres * sizeof(pixel_t));
	if (!*buf) return -1;
	res = png_image_finish_read(&image, 0, *buf, 0, NULL);
	if (!res) {
		if (errno) perror("png_image_finish_read");
		else fprintf(stderr, "png_image_finish_read %d\n", res);
		return res;
	}
	return 1;
}

int loadpalette(char *fn, unsigned long long **buf, unsigned long long *maxi)
{
	FILE *fh;
	char str[80], *str2;
	int sz = 1000;
	unsigned long long i;

	*maxi = 0;
	*buf = malloc(sz * sizeof(unsigned long long));
	if (!*buf) {
		perror("out of memory");
		return 0;
	}
	memset(*buf, 0, sz * sizeof(unsigned long long));
	fh = fopen(fn, "r");
	if (!fh) {
		perror("open palette file");
		return 0;
	}
	while(!feof(fh)) {
		if (fgets(str, sizeof(str), fh)) {
			i = strtoll(str, &str2, 0);
			if (i > *maxi) *maxi = i;
			while (i >= sz) {
				sz *= 2;
				*buf = realloc(*buf, sz * sizeof(unsigned long long));
				if (!*buf) {
					perror("out of memory");
					return 0;
				}
			}
			if (str2) {
				while ((' ' == *str2) || ('\t' == *str2)) str2++;
				(*buf)[i] = strtoll(str2, NULL, 0);
			}
		}
	}
	fclose(fh);
	return 1;
}
