/* All the heavy computing happens here */

#include <stddef.h>
#include <stdio.h>

#include "common.h"

unsigned long long mandelbrot(parameters_t *parameters, complex z, complex c)
{
	unsigned long long i;

	for(i = 0; i < parameters->maxiter; i++) {
		z = cpowl(z, parameters->exponent) + c;
		if (creall(z) * creall(z) + cimagl(z) * cimagl(z) > 10) break;
	}
	return i;
}

/* gets run as threads */
void *worker(void *data)
{
	job_t *job = data;
	unsigned long long i;
	unsigned long xpixel;
	long double x;

	for (xpixel = 0; xpixel < job->parameters->xres; xpixel++)
	{
		x = job->parameters->x1 + xpixel * (job->parameters->x2 - job->parameters->x1) / (job->parameters->xres-1);
		//FIXME mandel/julia switch here
		i = job->render(job->parameters, 0, x + I * job->y);
		job->line[xpixel].r = i & 0xffff;
		job->line[xpixel].g = (i >> 16) & 0xffff;
		job->line[xpixel].b = (i >> 32) & 0xffff;
	}
	//FIXME clean up?
	job->state = job_finished;
	return NULL;
}
