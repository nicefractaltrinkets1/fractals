/* common data structures */

#include <stdint.h>
#include <pthread.h>
#include <complex.h>

/* Global parameters */
typedef struct {
	char *infile, *outfile, *palfile;
	unsigned long xres, yres;	/* png file format limits us to 32bit! */
	unsigned threads;
	long double x1, x2, y1, y2;
	unsigned long long maxiter;
	unsigned exponent;		/* Mandelbrot for z^exponent+c */
} parameters_t;

/* a pixel in 16 bit RGB */
typedef struct __attribute__((__packed__)) {
	uint16_t r, g, b;
} pixel_t;

/* possible job states for workers */
typedef enum {
	job_ready,
	job_running,
	job_finished
} jobstate_t;

/* the render function */
typedef unsigned long long renderfunc_t(parameters_t *parameters, complex z, complex c);

/* data needed to calculate one line */
typedef struct {
	pthread_t id;
	parameters_t *parameters;
	renderfunc_t *render;
	jobstate_t state;
	long double y;
	pixel_t *line;
} job_t;

/* prototypes for all render functions */
unsigned long long mandelbrot(parameters_t *parameters, complex z, complex c);

/* prototype for worker */
void *worker(void *data);

/* png routines */
int savepng(char *fn, parameters_t *parameters, pixel_t *buf);
int loadpng(char *fn, parameters_t *parameters, pixel_t **buf);
int loadpalette(char *fn, unsigned long long **buf, unsigned long long *maxi);
