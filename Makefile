# simple Makefile, all done by implicit rules

CFLAGS+=-Wall -O2 -g -pthread
LDFLAGS+=-lm -lpng -pthread

SRC=fractals.c main.c png.c

all: fractals

fractals: $(SRC:.c=.o)

clean:
	rm -f *.o

distclean: clean
	rm -f fractals

depend:
	makedepend -Y. $(CFLAGS) $(SRC)
# DO NOT DELETE

threadfractals.o: common.h
threadmain.o: common.h
threadpng.o: common.h
